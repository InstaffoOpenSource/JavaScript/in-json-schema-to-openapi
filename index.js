#!/usr/bin/env node

const toOpenApi = require("json-schema-to-openapi-schema");
const fs = require("fs");

if (process.argv.length < 4) {
  console.log("Usage: npx @instaffogmbh/json-schema-to-openapi INPUT OUTPUT");
  process.exit(1);
}

const [_1, _2, input, output] = process.argv;

fs.readFile(input, "utf8", function(err, data) {
  const schema = JSON.parse(data);
  const convertedSchema = toOpenApi(schema);

  fs.writeFile(
    output,
    JSON.stringify(convertedSchema, null, 2),
    "utf8",
    () => null
  );
});
