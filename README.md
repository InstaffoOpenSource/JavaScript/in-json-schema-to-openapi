<!--#echo json="package.json" key="name" underline="=" -->

# @instaffogmbh/json-schema-to-openapi

<!--/#echo -->

<!--#echo json="package.json" key="description" -->

Coverts JSON Schema into Open API v3 format.

<!--/#echo -->

- 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).

## Usage

CLI: `npx @instaffogmbh/json-schema-to-openapi input.json output.json`

## Libraries

See: https://github.com/wework/json-schema-to-openapi-schema

## License

<!--#echo json="package.json" key=".license" -->

MIT

<!--/#echo -->
